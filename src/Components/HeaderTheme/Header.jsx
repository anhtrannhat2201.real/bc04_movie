import React from "react";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="shadow-2xl">
      {" "}
      <div className="h-20 flex justify-between items-center mx-auto container">
        <span className="text-yellow-500 text-2xl animate-bounce">XFlix</span>
        <UserNav />
      </div>
    </div>
  );
}
