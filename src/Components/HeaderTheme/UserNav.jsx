import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../../Services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let handleLogout = () => {
    // xóa data từ localStorage
    localServ.user.remove();
    //** */ remove data từ redux
    // dispatch({
    // type:SET_USER
    // payload:null

    window.location.href = "/login";
  };
  console.log("user: ", user);
  let renderContent = () => {
    if (user) {
      return (
        <>
          <span className="font-medium text-blue-800 underline text-4xl">
            {user.hoTen}
          </span>

          <button
            onClick={() => {
              handleLogout();
            }}
            className=" border rounded  bg-red-400  px-7 py-2 text-yellow-50"
          >
            Đăng Xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <div>
            {/* chuyển trang sử dụng NavLink để chuyển trang bằng to="/login" */}
            <NavLink to="/login">
              <button className="border rounded border-black px-5 py-2 bg-red-400 text-yellow-50 m-5 hover:bg-black hover:text-white transition ">
                Đăng nhập
              </button>
            </NavLink>
            <button className=" border rounded  bg-red-400  px-7 py-2 text-yellow-50">
              Đăng kí
            </button>
          </div>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
