import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  setLoadingOffAction,
  setLoadingOnAction,
} from "../../redux/actions/actionSpinner";
import { movieServ } from "../../Services/movieService";
import ItemMovie from "./ItemMovie";
import TabsMovies from "./TabsMovies";

export default function HomePage() {
  const [movies, setMovies] = useState([]);
  let dispatch = useDispatch();
  // const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    
    dispatch(setLoadingOnAction());
    // setIsLoading(true);
    movieServ
      .getListMovie()
      .then((res) => {
        console.log(res);
        // setIsLoading(false);
        dispatch(setLoadingOffAction());
        setMovies(res.data.content);
      })
      .catch((err) => {
        // setIsLoading(false);
        dispatch(setLoadingOffAction());
        console.log(err);
      });
  }, []);

  const renderMovies = () => {
    return movies.map((data, index) => {
      return <ItemMovie key={index} data={data} />;
    });
  };
  // thunk
  return (
    <div className="container mx-auto">
      <div className="grid grid-cols-5 gap-10 ">{renderMovies()}</div>

      <br />
      <br />
      <br />
      <br />
      <TabsMovies />

      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}
