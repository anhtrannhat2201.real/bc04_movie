import { Card } from "antd";

import React from "react";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function ItemMovie({ data = {} }) {
  return (
    <div>
      <Card
        hoverable
        style={{
          width: "100%",
        }}
        cover={
          <img
            className="h-80 w-full object-cover"
            alt="example"
            src={data.hinhAnh}
          />
        }
      >
        <Meta
          title={<p className="text-red-500 truncate">{data.tenPhim}</p>}
          description="www.instagram.com"
        />
        <NavLink to={`/detail/${data.maPhim}`}>
          <button className="w-full py-2 text-center bg-blue-500 text-white mt-5 rounded transition duration-500 hover:bg-black">
            Xem Chi Tiết
          </button>
        </NavLink>
      </Card>
    </div>
  );
}
