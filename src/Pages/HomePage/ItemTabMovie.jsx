import moment from "moment";
import React from "react";

export default function ItemTabMovie({ data }) {
  return (
    <div className="p-3 flex space-x-5 border-b border-red-500">
      <img
        className="w-28 h-36 object-cover"
        src={data.hinhAnh}
        alt="not data movie"
      />
      <div className="flex-grow">
        <p>{data.tenPhim}</p>
        {/* map lichj chiếu */}
        <div className="grid grid-cols-3 gap-5 mx-auto">
          {data.lstLichChieuTheoPhim.slice(0 - 9).map((gioChieu) => {
            return (
              <div className="p-3 rounded bg-red-500 text-white">
                {moment(gioChieu.ngayChieuGioChieu).format(
                  "DD-MM-YYYY ~ hh:mm"
                )}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
