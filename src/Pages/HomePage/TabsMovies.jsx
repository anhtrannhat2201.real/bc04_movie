import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieServ } from "../../Services/movieService";
import ItemMovie from "./ItemMovie";
import ItemTabMovie from "./ItemTabMovie";

export default function TabsMovies() {
  const [dataMovies, setDataMovies] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        console.log(res);
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderContent = () => {
    return dataMovies.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-16 h-16" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs tabPosition="left">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="w-48 text-left">
                      <p className="text-gray-700 truncate">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="truncate">{cumRap.diaChi}</p>
                    </div>
                  }
                  key={index}
                >
                  <div
                    className="h-32 scrollbar scrollbar-thumb-blue-700 scrollbar-track-blue-300 overflow-y-scroll hover:scrollbar-thumb-green-700"
                    style={{ height: 600, overflowY: "scroll" }}
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMovie data={phim} />;
                    })}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <div className=" ">
      <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
