import { localServ } from "../../Services/localService";
import { userServ } from "../../Services/userService";
import { SET_USER } from "../constants/constantUser";

const setUserLoginSuccess = (successValue) => {
  return {
    type: SET_USER,
    payload: successValue,
  };
};
const setUserLoginFail = (failValue) => {
  return {
    type: SET_USER,
    payload: failValue,
  };
};
// setUserActionService dùng khi có gọi api
export const setUserLoginActionServ = (
  dataLogin,
  onLoginSuccess,
  onLoginFail
) => {
  return (dispatch) => {
    userServ
      .postLogin(dataLogin)
      .then((res) => {
        // console.log(res);
        // dispatch({
        //   type: SET_USER,
        //   payload: res.data.content,
        // });
        localServ.user.set(res.data.content);
        onLoginSuccess();
        dispatch(setUserLoginSuccess(res.data.content));
      })
      .catch((err) => {
        // console.log(err);
        onLoginFail();
        dispatch(setUserLoginFail(err));
      });
  };
};
